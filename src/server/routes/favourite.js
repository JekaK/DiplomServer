const express = require('express');
const router = express.Router();
const knex = require('../db/connection');

const authHelpers = require('../auth/_helpers');
const util = require('../util/util');
const request = require('request');
const dbToken = require('../const/token.js').tokenDB;
/**
 * Метод для додавання фільму до улюбленого
 * 1. Ім'я шляху до запиту
 * 2. Функція перевірки токену користувача на валідність
 * 3. Функція - колбек
 * **/
router.post('/addToFavourite', authHelpers.tokenRequired, (req, res) => {
    request({
        method: 'GET',
        url: `https://api.themoviedb.org/3/movie/${req.body.movie_id}?api_key=${dbToken}`,
        headers: [
            {
                name: 'content-type',
                value: 'application/json;charset=utf-8'
            }
        ]
    }, (err, resp, body) => {
        let b = JSON.parse(body);
        knex.select()
            .from('favourite_movie')
            .where({
                user_token: req.headers.token,
                movie_id: b.id
            })
            .then(r => {
                if (r.length === 0) {
                    knex('favourite_movie')
                        .insert({
                            user_token: req.headers.token,
                            movie_info: b,
                            movie_id: b.id
                        }).returning('id')
                        .then(id => {
                            handleResponse(res, 200, `${JSON.stringify({id: id[0]})}`)
                        })
                        .catch(err => {
                            handleResponse(res, 500, `${err}`)
                        });
                }
                else {
                    handleResponse(res, 500, `${JSON.stringify({err: "Already added"})}`)
                }
            })
            .catch(err => {
                handleResponse(res, 500, `${err}`)
            });

    })
});

/**
 * Метод для видалення фільму зі списку улюблених
 * 1. Ім'я шляху до запиту
 * 2. Функція перевірки токену користувача на валідність
 * 3. Функція - колбек
 * **/
router.post('/removeFromFavourite', authHelpers.tokenRequired, (req, res) => {

    knex('favourite_movie')
        .where({
            user_token: req.headers.token,
            movie_id: req.body.movie_id
        })
        .del()
        .then(id => {
            handleResponse(res, 200, `${JSON.stringify(id)}`)
        })
        .catch(err => {
            handleResponse(res, 500, `${JSON.stringify(err)}`)
        });
});
/**
 * Метод для отримання списку улюблених фільмів
 * 1. Ім'я шляху до запиту
 * 2. Функція перевірки токену користувача на валідність
 * 3. Функція - колбек
 * **/
router.post('/getFavourite', authHelpers.tokenRequired, (req, res) => {
    knex.select()
        .from('favourite_movie')
        .where({
            user_token: req.headers.token
        })
        .then(r => {
            handleResponse(res, 200, {results: r})
        })
});

function handleResponse(res, code, statusMsg) {
    res.status(code).send(statusMsg)
}

module.exports = router;