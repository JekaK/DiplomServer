const express = require('express');
const router = express.Router();

const authHelpers = require('../auth/_helpers');
const passport = require('../auth/local');
var admin = require('firebase-admin');
var serviceAccount = require('../config/firebase_key');

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: 'https://filmaniachatapp.firebaseio.com/'
});
var db = admin.database();
var ref = db.ref("server/saving-data/firechat");
var usersRef = ref.child("users");

/**
 * Метод для реєстрації користувача
 * 1. Ім'я шляху до запиту
 * 2. Функція перевірки користувача на логінізіцію та зареєстрованість в ситсемі
 * 3. Функція - колбек
 * **/
router.post('/register', authHelpers.loginRedirect, (req, res, next) => {
    return authHelpers.createUser(req, res)
        .then((response) => {
            passport.authenticate('local', (err, user, info) => {
                if (user) {
                    usersRef.push().set({
                            username: req.body.username,
                            password: req.body.password
                        }
                    );
                    handleResponse(res, 200, {
                        success: true,
                        token: user.secret_key
                    });
                }
            })(req, res, next);
        })
        .catch((err) => {
            handleResponse(res, 500, 'error');
        });
});
/**
 * Метод для авторизації користувача
 * 1. Ім'я шляху до запиту
 * 2. Функція перевірки користувача на логінізіцію та зареєстрованість в ситсемі
 * 3. Функція - колбек
 * **/
router.post('/login', authHelpers.loginRedirect, (req, res, next) => {
    passport.authenticate('local', (err, user, info) => {
        if (err) {
            handleResponse(res, 500, 'error');
        }
        if (!user) {
            handleResponse(res, 404, 'User not found');
        }
        if (user) {
            req.logIn(user, function (err) {
                if (err) {
                    handleResponse(res, 500, 'error');
                }
                handleResponse(res, 200, {key: user.secret_key});
            });
        }
    })(req, res, next);
});
/**
 * Метод для деавторизації користувача
 * 1. Ім'я шляху до запиту
 * 2. Функція перевірки користувача на логінізіцію та зареєстрованість в ситсемі
 * 3. Функція - колбек
 * **/
router.get('/logout', authHelpers.loginRequired, (req, res, next) => {
    req.logout();
    handleResponse(res, 200, 'success');
});

function handleLogin(req, user) {
    return new Promise((resolve, reject) => {
        req.login(user, (err) => {
            if (err) reject(err);
            resolve();
        });
    });
}

/**
 * Функція, яка відправляє відповідь користувачу
 * **/
function handleResponse(res, code, statusMsg) {
    res.status(code).json({status: statusMsg});
}

module.exports = router;
