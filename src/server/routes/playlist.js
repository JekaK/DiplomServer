const express = require('express');
const router = express.Router();
const request = require('request');
const knex = require('../db/connection');
const authHelpers = require('../auth/_helpers');
const dbToken = require('../const/token.js').tokenDB;
/**
 * Метод для створення плейлиста
 * 1. Ім'я шляху до запиту
 * 2. Функція перевірки токену користувача на валідність
 * 3. Функція - колбек
 * **/
router.post('/createPlaylist', authHelpers.tokenRequired, (req, res) => {
    knex('playlists')
        .insert({
            name: req.body.name,
            user_key: req.headers.token
        }).returning('id')
        .then(id => {
            handleResponse(res, 200, `${id}`)
        })
        .catch(err => {
            handleResponse(res, 500, `${err}`)
        })
});
/**
 * Метод для видалення плейлиста
 * 1. Ім'я шляху до запиту
 * 2. Функція перевірки токену користувача на валідність
 * 3. Функція - колбек
 * **/
router.post('/deletePlaylist', authHelpers.tokenRequired, (req, res) => {
    knex('playlists')
        .where({
            id: req.body.id
        })
        .del()
        .then(id => {
            handleResponse(res, 200, `${id}`)
        })
        .catch(err => {
            handleResponse(res, 500, `${err}`)
        })
});
/**
 * Метод для додавання фільму в обраний плейлист
 * 1. Ім'я шляху до запиту
 * 2. Функція перевірки токену користувача на валідність
 * 3. Функція - колбек
 * **/
router.post('/addToPlaylist', authHelpers.tokenRequired, (req, res) => {
    request({
        method: 'GET',
        url: `https://api.themoviedb.org/3/movie/${req.body.movie_id}?api_key=${dbToken}`,
        headers: [
            {
                name: 'content-type',
                value: 'application/json;charset=utf-8'
            }
        ]
    }, (err, resp, body) => {
        let b = JSON.parse(body);
        knex.select('*')
            .from('playlist_movie')
            .where({
                playlist_id: req.body.playlist_id,
                movie_info: b,
                movie_id: b.id
            })
            .then((r) => {
                if (r.length !== 0) {
                    handleResponse(res, 500, `Already in db`)
                } else {
                    knex('playlist_movie')
                        .insert({
                            playlist_id: req.body.playlist_id,
                            movie_info: b,
                            movie_id: b.id
                        }).returning('id')
                        .then(id => {
                            handleResponse(res, 200, `${id}`)
                        })
                        .catch(err => {
                            handleResponse(res, 500, `${err}`)
                        });
                }
            })
            .catch(err => {
                handleResponse(res, 500, `${err}`)
            })
    })
});
/**
 * Метод для видалення фільму з обраного плейлиста
 * 1. Ім'я шляху до запиту
 * 2. Функція перевірки токену користувача на валідність
 * 3. Функція - колбек
 * **/
router.post('/deleteFromPlaylist', authHelpers.tokenRequired, (req, res) => {
    knex('playlist_movie')
        .where({
            playlist_id: req.body.playlist_id,
            movie_id: req.body.movie_id
        })
        .del()
        .then(id => {
            if (id !== 0)
                handleResponse(res, 200, `${id}`);
            if (id === 0) {
                handleResponse(res, 500, `${err}`)
            }
        })
        .catch(err => {
            handleResponse(res, 500, `${err}`)
        })
});
/**
 * Метод для редагування назви плейлиста
 * 1. Ім'я шляху до запиту
 * 2. Функція перевірки токену користувача на валідність
 * 3. Функція - колбек
 * **/
router.post('/editPlaylistName', authHelpers.tokenRequired, (req, res) => {
    knex('playlists')
        .update({
            name: req.body.name
        })
        .where({user_key: req.headers.token})
        .andWhere({
            id: req.body.playlist_id
        })
        .returning('id')
        .then(id => {
            handleResponse(res, 200, `${id}`)
        })
        .catch(err => {
            handleResponse(res, 500, `${err}`)
        })
});
/**
 * Метод для отримання списку плейлистів
 * 1. Ім'я шляху до запиту
 * 2. Функція перевірки токену користувача на валідність
 * 3. Функція - колбек
 * **/
router.post('/getPlaylists', authHelpers.tokenRequired, (req, res) => {
    knex.select('*')
        .from('playlists')
        .where({
            user_key: req.headers.token
        })
        .then((r) => {
            handleResponse(res, 200, r)
        })
        .catch(err => {
            handleResponse(res, 500, `${err}`)
        })
});

/**
 * Метод для отримання списку фільмів з обраного плейлиста
 * 1. Ім'я шляху до запиту
 * 2. Функція перевірки токену користувача на валідність
 * 3. Функція - колбек
 * **/
router.post('/getPlaylistMovies', authHelpers.tokenRequired, (req, res) => {
    knex.select('*')
        .from('playlist_movie')
        .where({
            playlist_id: req.body.playlist_id
        })
        .then((r) => {
            for (let i = 0; i < r.length; i++) {
                r[i].movie_info = JSON.parse(r[i].movie_info)
            }
            handleResponse(res, 200, r)
        })
        .catch(err => {
            handleResponse(res, 500, `${err}`)
        })
});

function handleResponse(res, code, statusMsg) {
    res.status(code).json({status: statusMsg});
}

module.exports = router;