const express = require('express');
const router = express.Router();
const knex = require('../db/connection');
var multer = require('multer');
var path = require('path');
var staticPathUser = path.resolve(__dirname, '..', 'static');
const fs = require('fs');

var storageUser = multer.diskStorage(
    {
        destination: staticPathUser,
        filename: function (req, file, cb) {
            cb(null, file.originalname);
        }
    }
);
var uploadUser = multer({storage: storageUser}).single('photo');

const authHelpers = require('../auth/_helpers');
/**
 * Метод для отримання інформації про користувача
 * 1. Ім'я шляху до запиту
 * 2. Функція перевірки токену користувача на валідність
 * 3. Функція - колбек
 * **/
router.post('/user', authHelpers.tokenRequired, (req, res, next) => {
    knex
        .select('*')
        .from('users')
        .where({
            secret_key: req.headers.token
        })
        .then(rs => {
            knex
                .select('*')
                .from('users_info')
                .where({
                    user_unique_id: rs[0].id
                })
                .then(r => {
                    knex('favourite_movie')
                        .count()
                        .where({
                            user_token: req.headers.token
                        })
                        .then(r => {
                            knex('playlists')
                                .count()
                                .where({
                                    user_key: req.headers.token
                                })
                                .then(p => {
                                    handleResponse(res, 200, {
                                        username: rs[0].username,
                                        email: r[0].email,
                                        photo: r[0].photo,
                                        playlist_c: parseInt(p[0].count),
                                        favourite_c: parseInt(r[0].count)
                                    })
                                })
                        })


                })
        })
});
/**
 * Метод для оновлення інформації про користувача
 * 1. Ім'я шляху до запиту
 * 2. Функція перевірки токену користувача на валідність
 * 3. Функція - колбек
 * **/
router.post('/updateInfo', authHelpers.tokenRequired, (req, res, next) => {
    if (req.body.username !== undefined) {
        knex('users')
            .update({
                username: req.body.username
            })
            .where('secret_key', req.headers.token)
            .returning('id')
            .then((r) => {
                if (req.body.email !== undefined) {
                    knex.select()
                        .from('users')
                        .where({
                            secret_key: req.headers.token
                        })
                        .then(rs => {
                            knex('users_info')
                                .update({
                                    email: req.body.email
                                })
                                .where('user_unique_id', rs[0].id)
                                .then(r => {
                                    handleResponse(res, 200, r)
                                })
                        })

                } else
                    handleResponse(res, 200, r)
            })
    } else if (req.body.email !== undefined) {
        knex.select()
            .from('users')
            .where({
                secret_key: req.headers.token
            })
            .then(rs => {
                knex('users_info')
                    .update({
                        email: req.body.email
                    })
                    .where('user_unique_id', rs[0].id)
                    .then(r => {
                        handleResponse(res, 200, r)
                    })
            })

    }

});
/**
 * Метод для оновлення фото користувача
 * 1. Ім'я шляху до запиту
 * 2. Функція перевірки токену користувача на валідність
 * 3. Функція - колбек
 * **/
router.post('/updatePhoto', authHelpers.tokenRequired, (req, res, next) => {
    uploadUser(req, res, (err) => {
        if (err) {
            return res.send(err)
        }
        var name = req.headers.token + '.jpg';
        fs.rename(path.join(staticPathUser, req.file.originalname), path.join(staticPathUser, name), function (err) {
            if (err) console.log('ERROR: ' + err);
        });
        knex.select()
            .from('users')
            .where({
                secret_key: req.headers.token
            })
            .then(rs => {
                knex('users_info')
                    .update({
                        photo: name
                    })
                    .where('user_unique_id', rs[0].id)
                    .then(r => {
                        handleResponse(res, 200, name)
                    })
            })
    });
});

function handleResponse(res, code, statusMsg) {
    res.status(code).json({status: statusMsg});
}

module.exports = router;
