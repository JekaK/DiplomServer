const express = require('express');
const router = express.Router();
const paginationMiddleware = require("express-pagination-middleware");

const authHelpers = require('../auth/_helpers');
const util = require('../util/util');
const request = require('request');
const dbToken = require('../const/token.js').tokenDB;
/**
 * Метод для отримання списку нових фільмів. Ця функція пітримує пагінцію
 * 1. Ім'я шляху до запиту
 * 2. Функція перевірки токену користувача на валідність
 * 3. Функція - колбек
 * **/

router.post('/newFilms', authHelpers.tokenRequired, (req, res, next) => {
    request(
        {
            method: 'GET',
            url: 'https://api.themoviedb.org/3/movie/upcoming?api_key=' + dbToken + '&language=en-US&page=' + req.body.page,
            headers: [
                {
                    name: 'content-type',
                    value: 'application/json;charset=utf-8'
                }
            ]
        },
        (err, resp, body) => {
            let b = JSON.parse(body);
            util.genreIdChecker(b);
            handleResponse(res, 200, b)
        })
});
/**
 * Метод для пошуку фільмів в TheMovieDB
 * 1. Ім'я шляху до запиту
 * 2. Функція перевірки токену користувача на валідність
 * 3. Функція - колбек
 * **/
router.post('/search', authHelpers.tokenRequired, (req, res) => {
    request({
        method: 'GET',
        url: `http://api.themoviedb.org/3/search/movie?api_key=${dbToken}&query=${req.body.query}&page=${req.body.page}`,
        headers: [
            {
                name: 'content-type',
                value: 'application/json;charset=utf-8'
            }
        ]
    }, (err, resp, body) => {
        let b = JSON.parse(body);
        util.genreIdChecker(b);
        handleResponse(res, 200, b)
    })
});
/**
 * Метод для отримання детальної інформації про фільм
 * 1. Ім'я шляху до запиту
 * 2. Функція перевірки токену користувача на валідність
 * 3. Функція - колбек
 * **/
router.post('/getMovieInfo', authHelpers.tokenRequired, (req, res) => {
    request({
        method: 'GET',
        url: `https://api.themoviedb.org/3/movie/${req.body.movie_id}?api_key=${dbToken}`,
        headers: [
            {
                name: 'content-type',
                value: 'application/json;charset=utf-8'
            }
        ]
    }, (err, resp, body) => {
        let b = JSON.parse(body);
        handleResponse(res, 200, {result: b})
    })
});

function handleResponse(res, code, statusMsg) {
    res.status(code).json(statusMsg)
}

module.exports = router;
