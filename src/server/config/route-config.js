(function (routeConfig) {

    'use strict';

    routeConfig.init = function (app) {

        // *** routes *** //
        const routes = require('../routes/index');
        const authRoutes = require('../routes/auth');
        const userRoutes = require('../routes/user');
        const filmRoutes = require('../routes/news');
        const favouriteRoutes = require('../routes/favourite');
        const playlistsRoutes = require('../routes/playlist');
        // *** register routes *** //
        app.use('/', routes);
        app.use('/auth', authRoutes);
        app.use('/', userRoutes);
        app.use('/', filmRoutes);
        app.use('/', favouriteRoutes);
        app.use('/', playlistsRoutes);
    };
})(module.exports);
