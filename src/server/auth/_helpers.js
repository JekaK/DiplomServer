const bcrypt = require('bcryptjs');
const knex = require('../db/connection');
const jwt = require('jwt-simple');
const genre = require('../const/genreId').genre;


function comparePass(userPassword, databasePassword) {
    return bcrypt.compareSync(userPassword, databasePassword);
}

function createUser(req, res) {
    return handleErrors(req)
        .then(() => {
            const salt = bcrypt.genSaltSync();
            const hash = bcrypt.hashSync(req.body.password, salt);
            let token = jwt.encode({
                username: req.body.username
            }, process.env.SECRET_KEY);

            return knex('users')
                .insert({
                    username: req.body.username,
                    password: hash,
                    secret_key: token
                })
                .returning('id')
                .then(id => {
                    return knex('users_info')
                        .insert({
                            user_unique_id: id[0]
                        }).returning('*')
                })
                .catch(err => {
                    res.status(400).json({status: err.message});
                })
        })
        .catch((err) => {
            res.status(400).json({status: err.message});
        });
}

function loginRequired(req, res, next) {
    if (!req.user) return res.status(401).json({status: 'Please log in'});
    return next();
}

function tokenRequired(req, res, next) {
    if (!req.headers.token)
        return res.status(401).json({status: 'Please, send token'});
    return next();
}

function isTokenExist(req, cb, errCb) {
    knex('users')
        .select()
        .where('secret_key', req.headers.token)
        .then((r) => {
            cb(r)
        })
        .catch(() => {
            errCb('Token doesn\'t exist');
        })
}

function loginRedirect(req, res, next) {
    if (req.user) return res.status(401).json(
        {status: 'You are already logged in'});
    return next();
}

function handleErrors(req) {
    return new Promise((resolve, reject) => {
        if (req.body.username.length < 6) {
            reject({
                message: 'Username must be longer than 6 characters'
            });
        }
        else if (req.body.password.length < 6) {
            reject({
                message: 'Password must be longer than 6 characters'
            });
        } else {
            resolve();
        }
    });
}


module.exports = {
    comparePass,
    createUser,
    loginRequired,
    loginRedirect,
    tokenRequired,
    isTokenExist
};
